//
//  SplitwiseTests.swift
//  SplitwiseTests
//
//  Created by Hoang Minh Truong on 3/28/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import XCTest
@testable import Splitwise

class SplitwiseTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    //MARK: - Test Participant Validator
    func testParticipantValidatorFail() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let participantViewModel = ParticipantViewModel()
        let participantModel = ParticipantModel(id: 1, name: "", dateCreate: "04/04/2019")
        let resultValidator: ValidatingParticipantResult = participantViewModel.validator(name: participantModel.name)
        let failEqual : Bool = (resultValidator == ValidatingParticipantResult.failed(reason: "Name is required."))
        //XCTAssertEqual(resultValidator,ValidatingParticipantResult.failed(reason: "Name is required"))
        XCTAssertEqual(true,failEqual)
    }
    
    //MARK: - Test Group Validator
    
    func testGroupValidator() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let groupViewModel = GroupViewModel()
        let groupModel = GroupModel(id: 1, description: "", dateCreate: "04/04/2019", participantsId: "", participantsName: "")
        let resultValidator: ValidatingGroupResult = groupViewModel.validator(groupModel: groupModel)
        let failEqual : Bool = (resultValidator == ValidatingGroupResult.failed(reason: "Please select participants is required and must more than 2 participants."))
        
        XCTAssertEqual(true,failEqual)
    }
    
    //MARK: - Test Group Expense Validator
    
    func testGroupExtenNoDateValidator() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let groupExpenseViewModel = GroupExpensesViewModel()
        let groupExpenseModel = GroupExpensesModel(id: 1, groupId: 1, description: "abc", dateCreate: "04/04/2019", amount: 1, participantsId: "1", participantsName: "name", participantsExtraAmount: "1", payerId: 1, payerName: "payer")
        let resultValidator: ValidatingExpenseResult = groupExpenseViewModel.validator(groupExpenseModel: groupExpenseModel)
        let failEqual : Bool = (resultValidator == ValidatingExpenseResult.failed(reason: "Please select date."))
        XCTAssertEqual(true,failEqual)
    }
    
    func testGroupExtenNoDescriptionValidator() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let groupExpenseViewModel = GroupExpensesViewModel()
        let groupExpenseModel = GroupExpensesModel(id: 1, groupId: 1, description: "", dateCreate: "04/04/2019", amount: 1, participantsId: "1", participantsName: "name", participantsExtraAmount: "1", payerId: 1, payerName: "payer")
        let resultValidator: ValidatingExpenseResult = groupExpenseViewModel.validator(groupExpenseModel: groupExpenseModel)
        let failEqual : Bool = (resultValidator == ValidatingExpenseResult.failed(reason: "Description is required."))
        XCTAssertEqual(true,failEqual)
    }
    
    func testGroupExtenNoAmountValidator() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let groupExpenseViewModel = GroupExpensesViewModel()
        let groupExpenseModel = GroupExpensesModel(id: 1, groupId: 1, description: "abc", dateCreate: "04/04/2019", amount: 0, participantsId: "1", participantsName: "name", participantsExtraAmount: "1", payerId: 1, payerName: "payer")
        let resultValidator: ValidatingExpenseResult = groupExpenseViewModel.validator(groupExpenseModel: groupExpenseModel)
        let failEqual : Bool = (resultValidator == ValidatingExpenseResult.failed(reason: "Total amount is required and number."))
        XCTAssertEqual(true,failEqual)
    }
    
    func testGroupExtenNoParticipantsValidator() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let groupExpenseViewModel = GroupExpensesViewModel()
        let groupExpenseModel = GroupExpensesModel(id: 1, groupId: 1, description: "abc", dateCreate: "04/04/2019", amount: 1, participantsId: "", participantsName: "", participantsExtraAmount: "1", payerId: 1, payerName: "payer")
        let resultValidator: ValidatingExpenseResult = groupExpenseViewModel.validator(groupExpenseModel: groupExpenseModel)
        let failEqual : Bool = (resultValidator == ValidatingExpenseResult.failed(reason: "Please select participants is required and must more than 2 participants."))
        XCTAssertEqual(true,failEqual)
    }
    
    func testGroupExtenNoPayerValidator() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let groupExpenseViewModel = GroupExpensesViewModel()
        let groupExpenseModel = GroupExpensesModel(id: 1, groupId: 1, description: "abc", dateCreate: "04/04/2019", amount: 1, participantsId: "1,2", participantsName: "name,name", participantsExtraAmount: "1,2", payerId: 0, payerName: "")
        let resultValidator: ValidatingExpenseResult = groupExpenseViewModel.validator(groupExpenseModel: groupExpenseModel)
        let failEqual : Bool = (resultValidator == ValidatingExpenseResult.failed(reason: "Payer is required."))
        XCTAssertEqual(true,failEqual)
    }
    
    func testGroupExtenPayerNotJoinValidator() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let groupExpenseViewModel = GroupExpensesViewModel()
        let groupExpenseModel = GroupExpensesModel(id: 1, groupId: 1, description: "abc", dateCreate: "04/04/2019", amount: 1, participantsId: "1,2", participantsName: "name,name", participantsExtraAmount: "1", payerId: 3, payerName: "payer")
        let resultValidator: ValidatingExpenseResult = groupExpenseViewModel.validator(groupExpenseModel: groupExpenseModel)
        let failEqual : Bool = (resultValidator == ValidatingExpenseResult.failed(reason: "Payer must joint in date."))
        XCTAssertEqual(true,failEqual)
    }
    
}
