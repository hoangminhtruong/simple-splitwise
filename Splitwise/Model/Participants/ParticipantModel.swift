//
//  participantModel.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 3/29/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation

class ParticipantModel {
    var id: Int = 0
    var name: String = ""
    var dateCreate: String = ""
    var isSelect: Bool = false
    var amountExpense: Int = 0
    
    init(id: Int,name: String,dateCreate: String) {
        self.id = id
        self.name = name
        self.dateCreate = dateCreate
    }
    
    init() {
    }
}
