//
//  GroupExpensesModel.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 4/2/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation

struct GroupExpensesModel {
    var id: Int = 0
    var groupId: Int = 0
    var description: String = ""
    var dateCreate: String = ""
    var amount: Int = 0
    var participantsId = ""
    var participantsName: String = ""
    var participantsExtraAmount: String = ""
    var payerId: Int = 0
    var payerName: String = ""
    
    init(id: Int, groupId: Int, description: String, dateCreate: String, amount: Int, participantsId: String, participantsName: String, participantsExtraAmount: String, payerId: Int, payerName: String) {
        self.id = id
        self.groupId = groupId
        self.description = description
        self.dateCreate = dateCreate
        self.amount = amount
        self.payerId = payerId
        self.payerName = payerName
        self.participantsId = participantsId
        self.participantsName = participantsName
        self.participantsExtraAmount = participantsExtraAmount
    }
    
    init() {
    }
}
