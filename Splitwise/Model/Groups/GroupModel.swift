//
//  GroupModel.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 3/29/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation

struct GroupModel {
    var id: Int = 0
    var description: String = ""
    var dateCreate: String = ""
    var participantsName = ""
    var participantsId = ""
    
    init(id: Int, description: String, dateCreate: String, participantsId: String, participantsName: String) {
        self.id = id
        self.description = description
        self.dateCreate = dateCreate
        self.participantsId = participantsId
        self.participantsName = participantsName
    }
    
    init() {
    }
}
