//
//  ManagerStoryBoard.swift
//  NUS
//
//  Created by tuan.nguyen on 2/23/17.
//  Copyright © 2017 kelvin. All rights reserved.
//

import UIKit

extension UIStoryboard {
    //MARK: - Main Storyboard
    class func mainStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }

    class func groupsViewController() -> GroupsViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "GroupsViewController") as? GroupsViewController
    }
    
    class func groupExpenseViewController() -> GroupExpenseViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "GroupExpenseViewController") as? GroupExpenseViewController
    }
    
    class func participantsViewController() -> ParticipantsViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "ParticipantsViewController") as? ParticipantsViewController
    }
    
    class func createGroupViewController() -> CreateGroupViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "CreateGroupViewController") as? CreateGroupViewController
    }
    
    class func createExpenseViewController() -> CreateExpenseViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "CreateExpenseViewController") as? CreateExpenseViewController
    }
}
