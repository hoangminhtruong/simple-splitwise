//
//  DateExtension.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 4/1/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation

extension Date {
    public func convertToFormat(format:String) -> String {
        let dtf = DateFormatter()
        dtf.timeZone = TimeZone.current
        dtf.dateFormat = format
        return dtf.string(from: self)
    }
}
