//
//  IntExtension.swift
//  Needii
//
//  Created by Mac on 6/11/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import Foundation
extension Int{
    func formatInt()->String{
        //return String(format: "%d", locale: Locale.current, self)
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .decimal
        // localize to your grouping and decimal separator
        currencyFormatter.locale = Locale.current
        
        // We'll force unwrap with the !, if you've got defined data you may need more error checking
        return currencyFormatter.string(from: NSNumber(value : self))!
    }
    
    func times(f: () -> ()) {
        if self > 0 {
            for _ in 0..<self {
                f()
            }
        }
    }
    
    func times( f: @autoclosure () -> ()) {
        if self > 0 {
            for _ in 0..<self {
                f()
            }
        }
    }
}
