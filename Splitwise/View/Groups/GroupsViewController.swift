//
//  GroupViewController.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 3/29/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import UIKit
import SQLite
import RxSwift
import RxCocoa

class GroupsViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    private let disposeBag = DisposeBag()
    private var groupViewModel = GroupViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        groupViewModel.fetch()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - User defined
    func setupViews() {
        title = "Groups"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(addTap))
        bindTableView()
        tableViewTap()
    }
    
    func bindTableView() {
        groupViewModel.groups.asObservable().bind(to: tblView.rx.items) {
            (tableView: UITableView, index: Int, element: GroupModel) in
                let cell = UITableViewCell(style: .default, reuseIdentifier:
                    "cell")
                cell.textLabel?.numberOfLines = 0
            cell.textLabel?.text = element.description != "" ? String.init(format: "%@ - %@", element.description,element.participantsName) : String.init(format: "%@", element.participantsName)
                cell.selectionStyle = .none
                return cell
            }
            .disposed(by: disposeBag)
    }
    
    //MARK: - Action
    @objc func addTap() {
        let vc = UIStoryboard.createGroupViewController()
        vc?.groupModel.asObservable().subscribe(onNext: { (group) in
            let resultValidator = self.groupViewModel.validator(groupModel: group)
            switch resultValidator {
                case .failed(let reason):
                    Utils.showAlert(title: "Create Group", message: reason, viewController: self)
                case .passed(let groupModel):
                    self.groupViewModel.insert(description: groupModel.description, participantsId: groupModel.participantsId, participantsName: groupModel.participantsName)
            }
        }, onCompleted: {
            
        })
        .disposed(by: disposeBag)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func tableViewTap() {
        tblView.rx.itemSelected.asObservable().subscribe(onNext: { (indexPath) in
            print("\(indexPath.row)")
            let vc = UIStoryboard.groupExpenseViewController()
            vc?.groupModel = self.groupViewModel.groups.value[indexPath.row]
            self.navigationController?.pushViewController(vc!, animated: true)
        })
            .disposed(by: disposeBag)
    }
}
