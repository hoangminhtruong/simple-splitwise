//
//  CreateGroupViewController.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 4/2/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CreateGroupViewController: UIViewController {
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var lblparticipant: UILabel!
    
    var groupModel : PublishSubject<GroupModel> = PublishSubject<GroupModel>()
    private let disposeBag = DisposeBag()
    private var participantsId = "", participantsName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    //MARK: - User defined
    func setupViews() {
        self.title = "Create Group"
    }
    
    //MARK: - Action
    
    @IBAction func submitAction(_ sender: Any) {
        groupModel.onNext(GroupModel(id: 0, description: txtDescription.text!, dateCreate: "", participantsId: participantsId, participantsName: participantsName))
        groupModel.onCompleted()
        groupModel.disposed(by: DisposeBag())
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectPaticipants(_ sender: Any) {
        let vc = UIStoryboard.participantsViewController()
        vc?.typeSelect = .group
        vc?.participantsId.asObserver()
            .subscribe(onNext: { (ids) in
                self.participantsId = ids
                print(ids)
            },onCompleted: {
                debugPrint("completed")
            })
            .disposed(by: disposeBag)
        vc?.participantsName.asObserver()
            .subscribe(onNext: { (names) in
                self.participantsName = names
                self.lblparticipant.text = names != "" ? names : "Select participants"
            },onCompleted: {
                debugPrint("completed")
            })
            .disposed(by: disposeBag)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
