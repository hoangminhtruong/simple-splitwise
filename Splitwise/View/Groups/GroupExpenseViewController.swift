//
//  GroupExpenseViewController.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 3/29/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import UIKit
import SQLite
import RxSwift
import RxCocoa

class GroupExpenseViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    var groupModel = GroupModel()
    private let disposeBag = DisposeBag()
    private var groupExpensesViewModel = GroupExpensesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        groupExpensesViewModel.fetch(groupId : groupModel.id)
        // Do any additional setup after loading the view.
    }
    
    //MARK: - User defined
    func setupViews() {
        title = groupModel.description
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(addTap))
        bindTableView()
    }
    
    func bindTableView() {
        groupExpensesViewModel.groupExpenses.asObservable().bind(to: tblView.rx.items) {
            (tableView: UITableView, index: Int, element: GroupExpensesModel) in
            let cell = UITableViewCell(style: .default, reuseIdentifier:
                "cell")
            let names = self.groupExpensesViewModel.getParticipantsName(names: element.participantsName, extraAmounts: element.participantsExtraAmount)
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.text = element.description != "" ? String.init(format: "%@ - %@ - $%@ - %@ - %@ paid.", element.dateCreate, element.description, element.amount.formatInt(), names , element.payerName) : String.init(format: "%@ - $%@ - %@ - %@ paid.", element.dateCreate, element.amount.formatInt(), names , element.payerName)
            cell.selectionStyle = .none
            return cell
            }
            .disposed(by: disposeBag)
    }
    
    //MARK: - Action
    @objc func addTap() {
        let vc = UIStoryboard.createExpenseViewController()
        vc?.groupExpensesModel.asObservable().subscribe(onNext: { (groupExpense) in
            let resultValidator = self.groupExpensesViewModel.validator(groupExpenseModel: groupExpense)
            switch resultValidator {
            case .failed(let reason):
                Utils.showAlert(title: "Create Expense", message: reason, viewController: self)
            case .passed(let groupExpenseModel):
                self.groupExpensesViewModel.insert(groupId: self.groupModel.id, date: groupExpenseModel.dateCreate,description: groupExpenseModel.description, amount: groupExpenseModel.amount, payerId: groupExpenseModel.payerId, payerName: groupExpenseModel.payerName, participantsId: groupExpenseModel.participantsId, participantsName: groupExpenseModel.participantsName, participantsExtraAmount: groupExpenseModel.participantsExtraAmount)
            }
        }, onCompleted: {
            
        })
            .disposed(by: disposeBag)
        vc?.groupModel = groupModel
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func viewOverral(_ sender: Any) {
        let participantViewModel = ParticipantViewModel()
        Utils.showAlert(title: "Overall balance", message: participantViewModel.getOverallBalance(groupId: self.groupModel.id, participantsId : self.groupModel.participantsId),viewController: self)
    }
    
    @IBAction func viewParticipants(_ sender: Any) {
        let vc = UIStoryboard.participantsViewController()
        vc?.typeSelect = .none
        vc?.groupId = groupModel.id
        vc?.participantsIdString = groupModel.participantsId
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
