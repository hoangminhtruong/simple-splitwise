//
//  CreateExpenseViewController.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 4/2/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ActionSheetPicker_3_0

class CreateExpenseViewController: UIViewController {
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var lblPayer: UILabel!
    @IBOutlet weak var lblParticipants: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    var groupExpensesModel : PublishSubject<GroupExpensesModel> = PublishSubject<GroupExpensesModel>()
    var groupModel: GroupModel!
    var groupExpenseViewModel = GroupExpensesViewModel()
    private let disposeBag = DisposeBag()
    private var payerId = "", payerName = "",participantsId = "", participantsName = "",participantsExtraAmount = "",dateSubmit = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    //MARK: - User defined
    func setupViews() {
        self.title = "Create Expense"
    }
    
    //MARK: - Action
    
    @IBAction func submitAction(_ sender: Any) {
        groupExpensesModel.onNext(GroupExpensesModel(id: 0, groupId: groupModel.id, description: txtDescription.text!, dateCreate: dateSubmit, amount: Int(txtAmount.text!) ?? 0, participantsId: participantsId, participantsName: participantsName, participantsExtraAmount: participantsExtraAmount, payerId: Int(payerId) ?? 0, payerName: payerName))
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectPaticipants(_ sender: Any) {
        self.view.endEditing(true)
        let vc = UIStoryboard.participantsViewController()
        vc?.typeSelect = .expense
        vc?.participantsIdString = groupModel.participantsId
        debugPrint(groupModel.participantsId)
        vc?.participantsId.asObserver()
            .subscribe(onNext: { (ids) in
                self.participantsId = ids
            
                print(ids)
            },onCompleted: {
                debugPrint("completed")
            })
            .disposed(by: disposeBag)
        vc?.participantsExtraAmount.asObserver()
            .subscribe(onNext: { (extraAmounts) in
                self.participantsExtraAmount = extraAmounts
                print(extraAmounts)
            },onCompleted: {
                debugPrint("completed")
            })
            .disposed(by: disposeBag)
        vc?.participantsName.asObserver()
            .subscribe(onNext: { (names) in
                self.participantsName = names
                self.lblParticipants.text = self.groupExpenseViewModel.getParticipantsName(names: names, extraAmounts: self.participantsExtraAmount)
            },onCompleted: {
                debugPrint("completed")
            })
            .disposed(by: disposeBag)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func selectPayer(_ sender: Any) {
        self.view.endEditing(true)
        let vc = UIStoryboard.participantsViewController()
        vc?.typeSelect = .payer
        vc?.participantsIdString = groupModel.participantsId
        vc?.participantsId.asObserver()
            .subscribe(onNext: { (ids) in
                self.payerId = ids
                print(ids)
            },onCompleted: {
                debugPrint("completed")
            })
            .disposed(by: disposeBag)
        vc?.participantsName.asObserver()
            .subscribe(onNext: { (names) in
                self.payerName = names
                self.lblPayer.text = names
            },onCompleted: {
                debugPrint("completed")
            })
            .disposed(by: disposeBag)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func selectDate(_ sender: Any) {
        self.view.endEditing(true)
        ActionSheetDatePicker.show(withTitle: "Select Date", datePickerMode: .date, selectedDate: Date(), doneBlock: { (picker, value, origin) in
            if let date = value as? Date{
                self.dateSubmit = date.convertToFormat(format: "MMM dd, yyyy")
                self.lblDate.text = self.dateSubmit
            }
        }, cancel: { (picker) in
            return
        }, origin: self.view)
    }
}
