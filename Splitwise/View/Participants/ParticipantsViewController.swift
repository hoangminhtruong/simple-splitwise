//
//  participantViewController.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 3/29/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import UIKit
import SQLite
import RxSwift
import RxCocoa

class ParticipantsViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var heightSelectConstraint: NSLayoutConstraint!
    var typeSelect: TypeSelect = .none
    var participantsIdString = ""
    var groupId: Int = 0
    var participantsId: PublishSubject<String> = PublishSubject<String>()
    var participantsName: PublishSubject<String> = PublishSubject<String>()
    var participantsExtraAmount: PublishSubject<String> = PublishSubject<String>()
    
    private let disposeBag = DisposeBag()
    private var participantViewModel = ParticipantViewModel()
    private let cellId = "participantCell"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - User defined
    func setupViews() {
        title = "Participants"
        tblView.register(UINib(nibName: "ParticipantCell", bundle: nil), forCellReuseIdentifier: cellId)
        if typeSelect != .none {
            self.heightSelectConstraint.constant = 40
            if typeSelect == .group{
                navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(addTap))
                self.participantViewModel.fetch()
            }else{
                self.participantViewModel.fetch(participantsId: participantsIdString)
            }
        } else {
            self.heightSelectConstraint.constant = 0
            self.participantViewModel.fetch(participantsId: participantsIdString)
        }
        bindTableView()
        tableViewTap()
    }
    
    func bindTableView() {
        participantViewModel.participants.asObservable().bind(to: tblView.rx.items) {
                (tableView: UITableView, index: Int, participant: ParticipantModel) in
                let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId) as! ParticipantCell
                cell.participantModel = participant
            cell.canSelect = self.typeSelect == .none ? false : true
                cell.selectionStyle = .none
                return cell
            }
            .disposed(by: disposeBag)
    }
    
    //MARK: - Action
    @objc func addTap() {
        let alert = UIAlertController(title: "Create Participant", message: nil, preferredStyle: .alert)
        alert.addTextField { (tf) in tf.placeholder = "Name" }
        let action = UIAlertAction(title: "Submit", style: .default) { (_) in
            guard let name = alert.textFields?.first?.text else { return }
            print(name)
            let resultValidator = self.participantViewModel.validator(name: name)
            switch resultValidator {
                case .failed(let reason):
                        Utils.showAlert(title: "Create Participant", message: reason, viewController: self)
                case .passed(let name):
                        self.participantViewModel.insert(name: name)
            }
            
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
        }
        alert.addAction(action)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
    }
    
    func tableViewTap() {
        tblView.rx.itemSelected.asObservable()
            .subscribe(onNext: { (indexPath) in
            print("\(indexPath.row)")
            if self.typeSelect != .none {
                var participants = self.participantViewModel.participants.value
                if self.typeSelect == .payer{
                    participants = participants.map{ elemet in
                        return ParticipantModel(id: elemet.id, name: elemet.name, dateCreate: elemet.dateCreate)
                    }
                }
                participants[indexPath.row].isSelect = !participants[indexPath.row].isSelect
                self.participantViewModel.participants.accept(participants)
            } else {
                Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false, block: { (timer) in
                    Utils.showAlert(title: "Expense diary", message: self.participantViewModel.getExpenseDiary(participant: self.participantViewModel.participants.value[indexPath.row], groupId: self.groupId),viewController: self)
                })
                
            }
        })
            .disposed(by: disposeBag)
    }
    
    @IBAction func selectAction(_ sender: Any) {
        let ptsArraySelected = participantViewModel.participants.value.filter{
            return $0.isSelect == true
        }
        if typeSelect == .expense{
            let alert = UIAlertController(title: "Extra Expense", message: "Fill if have any person have ordered an extra for themselves", preferredStyle: .alert)
            for item in ptsArraySelected{
                alert.addTextField { (tf) in tf.placeholder = "Extra money of " + item.name }
            }
            let action = UIAlertAction(title: "Submit", style: .default) { (_) in
                var participantsExtraAmount = ""
                for textfield in alert.textFields!{
                    participantsExtraAmount += participantsExtraAmount == "" ? "\(Int(textfield.text!) ?? 0)" : ",\(Int(textfield.text!) ?? 0)"
                }
                self.participantsExtraAmount.onNext(participantsExtraAmount)
                self.participantsExtraAmount.onCompleted()
                self.participantsExtraAmount.disposed(by: self.disposeBag)
                self.selected(ptsArraySelected)
            }
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }else{
           selected(ptsArraySelected)
        }
        
    }
    
    func selected(_ ptsArraySelected: [ParticipantModel]){
        self.navigationController?.popViewController(animated: true)
        let ptsName = ptsArraySelected.map{ participant in
            return participant.name
            }.joined(separator: ", ")
        let ptsId = ptsArraySelected.map{ participant in
            return "\(participant.id)"
            }.joined(separator: ",")
        participantsName.onNext(ptsName)
        participantsName.onCompleted()
        participantsName.disposed(by: disposeBag)
        participantsId.onNext(ptsId)
        participantsId.onCompleted()
        participantsId.disposed(by: disposeBag)
    }
}
