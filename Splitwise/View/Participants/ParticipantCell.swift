//
//  participantCell.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 4/2/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import UIKit

class ParticipantCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgSelect: UIImageView!
    @IBOutlet weak var leadingConstraintName: NSLayoutConstraint!
    @IBOutlet weak var widthContraintBtn: NSLayoutConstraint!
    
    var participantModel: ParticipantModel?{
        didSet{
            self.lblName.text = participantModel!.name
            self.imgSelect.image = UIImage(named: participantModel!.isSelect ? "icCheck" : "icUnCheck")
        }
    }
    
    var canSelect: Bool?{
        didSet{
            self.widthContraintBtn.constant = canSelect ?? false ? 15 : 0
            self.leadingConstraintName.constant = canSelect ?? false ? 10 : 0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
