//
//  participantViewModel.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 3/29/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation
import SQLite
import RxSwift
import RxCocoa

enum TypeSelect:Int{
    case none,group,payer,expense
}

enum ValidatingParticipantResult {
    case passed(name: String)
    case failed(reason: String)
    
    static func ==(lhs: ValidatingParticipantResult, rhs: ValidatingParticipantResult) -> Bool {
        switch (lhs, rhs) {
        case (let .failed(reason1), let .failed(reason2)):
            return reason1 == reason2
        default:
            return false
        }
    }
}

class ParticipantViewModel{
    var participants: BehaviorRelay<[ParticipantModel]> = BehaviorRelay(value: [])
    var database: Connection!
    
    let participantsTable = Table("participants")
    let id = Expression<Int>("id")
    let name = Expression<String>("name")
    let dateCreated = Expression<String>("date_created")
    
    //MARK: - SQLite
    init() {
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent("groups").appendingPathExtension("sqlite3")
            let database = try Connection(fileUrl.path)
            self.database = database
        } catch {
            print(error)
        }
        create()
    }
    
    func create() {
        let createTable = self.participantsTable.create { (table) in
            table.column(self.id, primaryKey: true)
            table.column(self.name)//unique: true
            table.column(self.dateCreated)
        }
        
        do {
            try self.database.run(createTable)
            print("Create successful")
        } catch {
            print(error)
        }
    }
    
    func fetch() {
        participants.accept([])
        do {
            let participants = try self.database.prepare(self.participantsTable)
            var participantsModel : [ParticipantModel] = []
            for participant in participants {
                print("participantId: \(participant[self.id]), name: \(participant[self.name]), date_created: \(participant[self.dateCreated])")
                participantsModel.append(ParticipantModel(id: participant[self.id],name: participant[self.name], dateCreate: participant[self.dateCreated]))
            }
            self.participants.accept(participantsModel)
        } catch {
            print(error)
        }
    }
    
    func fetch(participantsId: String) {
        let participantsIdArray = participantsId.split(separator: ",")
        var participantsModel : [ParticipantModel] = []
        participants.accept([])
        do {
            for id in participantsIdArray{
                let participants = try self.database.prepare(self.participantsTable.filter(self.id == Int(id) ?? 0))
                for participant in participants {
                    print("participantId: \(participant[self.id]), name: \(participant[self.name]), date_created: \(participant[self.dateCreated])")
                    participantsModel.append(ParticipantModel(id: participant[self.id],name: participant[self.name], dateCreate: participant[self.dateCreated]))
                }
            }
            self.participants.accept(participantsModel)
        } catch {
            print(error)
        }
    }
    
    func insert(name: String) {
        let dateString = Date().convertToFormat(format: "MMM dd")
        let insertGroup = self.participantsTable.insert(self.name <- name, self.dateCreated <- dateString)
        do {
            try self.database.run(insertGroup)
            self.fetch()
            print("Insert successful")
        } catch {
            print(error)
        }
    }
    
    func delete(id: Int) {
        let participant = self.participantsTable.filter(self.id == id)
        let deleteParticipant = participant.delete()
        do {
            try self.database.run(deleteParticipant)
        } catch {
            print(error)
        }
    }
    
    func update(id: Int, name: String) {
        let participant = self.participantsTable.filter(self.id == id)
        let dateString = Date().convertToFormat(format: "MMM dd")
        let updateparticipant = participant.update(self.name <- name, self.dateCreated <- dateString)
        do {
            try self.database.run(updateparticipant)
        } catch {
            print(error)
        }
    }
    
    //MARK: - Others
    
    func getExpenseDiary(participant: ParticipantModel, groupId: Int) -> String {
        var expenseDiaryString = ""
        let groupExpensesViewModel = GroupExpensesViewModel()
        groupExpensesViewModel.fetch(groupId: groupId)
        let expenseJoin = groupExpensesViewModel.groupExpenses.value.filter{
            let arrayId = $0.participantsId.components(separatedBy: ",")
            return $0.payerId == participant.id || $0.payerId != participant.id && arrayId.firstIndex(of: "\(participant.id)") != nil
        }
        
        for expense in expenseJoin{
            var expenseDiary = 0
            let arrayExtra = expense.participantsId.components(separatedBy: ",")
            let totalExtra = arrayExtra.reduce(0) { (result, element) -> Int in
                return result + (Int(element) ?? 0)
            }
            if expense.payerId == participant.id{
                expenseDiary = (expense.amount - totalExtra) / arrayExtra.count * (arrayExtra.count - 1)
            }else{
                expenseDiary = -(expense.amount - totalExtra) / arrayExtra.count
            }
            let stringFormat = expenseDiary >= 0 ? "%@ - %@ - %@ - $%@" : "%@ - %@ - %@ - -$%@"
            expenseDiaryString += expenseDiaryString == "" ? String.init(format: stringFormat, participant.name, expense.dateCreate, expense.description, abs(expenseDiary).formatInt()) : String.init(format: "; " + stringFormat, participant.name, expense.dateCreate, expense.description, abs(expenseDiary).formatInt())
        }
    
        return expenseDiaryString == "" ? "\(participant) dont have any expense or expense is balance" : expenseDiaryString
    }
    
    func getTotalExpenseParticipant(participant: ParticipantModel, groupId: Int) -> Int {
        var expenseDiary = 0
        let groupExpensesViewModel = GroupExpensesViewModel()
        groupExpensesViewModel.fetch(groupId: groupId)
        let expenseJoin = groupExpensesViewModel.groupExpenses.value.filter{
            let arrayId = $0.participantsId.components(separatedBy: ",")
            return $0.payerId == participant.id || $0.payerId != participant.id && arrayId.firstIndex(of: "\(participant.id)") != nil
        }
        
        for expense in expenseJoin{
            let arrayExtra = expense.participantsExtraAmount.components(separatedBy: ",")
            debugPrint("arrayExtra",arrayExtra)
            let totalExtra = arrayExtra.reduce(0) { (result, element) -> Int in
                return result + (Int(element) ?? 0)
            }
            if expense.payerId == participant.id{
                expenseDiary += (expense.amount - totalExtra) / arrayExtra.count * (arrayExtra.count - 1)
                debugPrint(participant.name,(expense.amount - totalExtra) / arrayExtra.count * (arrayExtra.count - 1),totalExtra)
            }else{
                expenseDiary += -(expense.amount - totalExtra) / arrayExtra.count
            }
        }
        
        debugPrint(participant.name,expenseDiary)
        return expenseDiary
    }
    
    func getOverallBalance(groupId: Int, participantsId : String) -> String {
        var overallBalance = ""
        let participantsViewModel = ParticipantViewModel()
        participantsViewModel.fetch(participantsId: participantsId)
        let participants = participantsViewModel.participants.value
        var listExpensePositive: [ParticipantModel] = []
        var listExpenseNegetive: [ParticipantModel] = []
        for participant in participants{
            let expense = getTotalExpenseParticipant(participant: participant, groupId: groupId)
            participant.amountExpense = expense
            if expense > 0 {
                listExpensePositive.append(participant)
            } else {
                listExpenseNegetive.append(participant)
            }
        }
        if listExpensePositive.count > 0 && listExpenseNegetive.count > 0 {
            overallBalance = getStatusOwes(listExpensePositive: listExpensePositive, listExpenseNegative: listExpenseNegetive)
        }
        
        return overallBalance == "" ? "Group dont have any expense" : overallBalance
    }
    
    func getStatusOwes(listExpensePositive: [ParticipantModel], listExpenseNegative: [ParticipantModel]) -> String {
        var statusOwes = ""
        var indexNegative = 0, indexPositive = 0
        
        while indexNegative < listExpenseNegative.count && indexPositive < listExpensePositive.count{
            let amountOwes = listExpenseNegative[indexNegative].amountExpense + listExpensePositive[indexPositive].amountExpense
            let formatString = statusOwes == "" ? "%@ owes %@ $%@" : ", %@ owes %@ $%@"
            statusOwes += String.init(format: formatString, listExpenseNegative[indexNegative].name,listExpensePositive[indexPositive].name,amountOwes > 0 ? abs(listExpenseNegative[indexNegative].amountExpense).formatInt() : abs(listExpenseNegative[indexNegative].amountExpense - amountOwes).formatInt())
            debugPrint("listExpenseNegative.count",listExpenseNegative[indexNegative].amountExpense,amountOwes)
            if amountOwes > 0 {
                listExpensePositive[indexPositive].amountExpense = amountOwes
               indexNegative += 1
            } else if amountOwes < 0 {
                indexPositive += 1
                listExpenseNegative[indexNegative].amountExpense -= amountOwes
            }else{
                indexNegative += 1
                indexPositive += 1
            }
        }
        
        return statusOwes
    }
    //MARK: - Validator
    
    func validator(name: String) -> ValidatingParticipantResult {
        if name.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            return .failed(reason: "Name is required.")
        }
        return .passed(name: name)
    }
}
