//
//  GroupExpenseViewModel.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 4/2/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation
import SQLite
import RxSwift
import RxCocoa

enum ValidatingExpenseResult {
    case passed(groupExpenseModel: GroupExpensesModel)
    case failed(reason: String)
    static func ==(lhs: ValidatingExpenseResult, rhs: ValidatingExpenseResult) -> Bool {
        switch (lhs, rhs) {
        case (let .failed(reason1), let .failed(reason2)):
            return reason1 == reason2
        default:
            return false
        }
    }
}

class GroupExpensesViewModel {
    var groupExpenses: BehaviorRelay<[GroupExpensesModel]> = BehaviorRelay(value: [])
    var database: Connection!
    
    let groupExpensesTable = Table("group_expenses")
    let id = Expression<Int>("id")
    let groupId = Expression<Int>("group_id")
    let description = Expression<String>("description")
    let dateCreated = Expression<String>("date_created")
    let amout = Expression<Int>("amount")
    let payerId = Expression<Int>("payer_id")
    let payerName = Expression<String>("payer_name")
    let participantsId = Expression<String>("participants_id")
    let participantsName = Expression<String>("participants_name")
    let participantsExtraAmount = Expression<String>("participants_extra_amout")
    
    init() {
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent("groups").appendingPathExtension("sqlite3")
            let database = try Connection(fileUrl.path)
            self.database = database
        } catch {
            print(error)
        }
        create()
    }
    
    func create() {
        let createTable = self.groupExpensesTable.create { (table) in
            table.column(self.id, primaryKey: true)
            table.column(self.groupId)
            table.column(self.description)
            table.column(self.dateCreated)
            table.column(self.amout)
            table.column(self.payerId)
            table.column(self.payerName)
            table.column(self.participantsId)
            table.column(self.participantsName)
            table.column(self.participantsExtraAmount)
        }
        
        do {
            try self.database.run(createTable)
            print("Create successful")
        } catch {
            print(error)
        }
    }
    
    func fetch() {
        groupExpenses.accept([])
        do {
            let groupExpenses = try self.database.prepare(self.groupExpensesTable)
            var groupExpensesModel : [GroupExpensesModel] = []
            for groupExpense in groupExpenses {
                print("groupId: \(groupExpense[self.id]), description: \(groupExpense[self.description]), date_created: \(groupExpense[self.dateCreated])")
                groupExpensesModel.append(GroupExpensesModel(id: groupExpense[self.id], groupId:groupExpense[self.groupId], description: groupExpense[self.description], dateCreate: groupExpense[self.dateCreated], amount: groupExpense[self.amout], participantsId: groupExpense[self.participantsId], participantsName: groupExpense[self.participantsName], participantsExtraAmount: groupExpense[self.participantsExtraAmount], payerId: groupExpense[self.payerId], payerName: groupExpense[self.payerName]))
            }
            self.groupExpenses.accept(groupExpensesModel)
        } catch {
            print(error)
        }
    }
    
    func fetch(groupId: Int) {
        groupExpenses.accept([])
        do {
            let groupExpenses = try self.database.prepare(self.groupExpensesTable.filter(self.groupId == groupId))
            var groupExpensesModel : [GroupExpensesModel] = []
            for groupExpense in groupExpenses {
                print("groupId: \(groupExpense[self.id]), description: \(groupExpense[self.description]), date_created: \(groupExpense[self.dateCreated])")
                groupExpensesModel.append(GroupExpensesModel(id: groupExpense[self.id], groupId:groupExpense[self.groupId], description: groupExpense[self.description], dateCreate: groupExpense[self.dateCreated], amount: groupExpense[self.amout], participantsId: groupExpense[self.participantsId], participantsName: groupExpense[self.participantsName], participantsExtraAmount: groupExpense[self.participantsExtraAmount], payerId: groupExpense[self.payerId], payerName: groupExpense[self.payerName]))
            }
            self.groupExpenses.accept(groupExpensesModel)
        } catch {
            print(error)
        }
    }
    
    func insert(groupId: Int, date: String, description: String,amount: Int, payerId: Int, payerName: String, participantsId: String,participantsName: String,participantsExtraAmount: String) {
        let dateString = Date().convertToFormat(format: "MMM dd")
        let insertGroupExpense = self.groupExpensesTable.insert(self.groupId <- groupId, self.dateCreated <- date ,self.description <- description, self.dateCreated <- dateString, self.amout <- amount, self.payerId <- payerId, self.payerName <- payerName, self.participantsId <- participantsId, self.participantsName <- participantsName, self.participantsExtraAmount <- participantsExtraAmount)
        do {
            try self.database.run(insertGroupExpense)
            self.fetch(groupId : groupId)
            print("Insert successful")
        } catch {
            print(error)
        }
    }
    
    func delete(id: Int) {
        let groupExpense = self.groupExpensesTable.filter(self.id == id)
        let deleteGroupExpense = groupExpense.delete()
        do {
            try self.database.run(deleteGroupExpense)
        } catch {
            print(error)
        }
    }
    
    func update(id: Int, date: String, description: String,amount: Int, payerId: Int, payerName: String, participantsId: String,participantsName: String,participantsExtraAmount: String) {
        let groupExpense  = self.groupExpensesTable.filter(self.id == id)
        let dateString = Date().convertToFormat(format: "MMM dd")
        let updateGroupExpense = groupExpense .update(self.dateCreated <- date ,self.description <- description, self.dateCreated <- dateString, self.amout <- amount, self.payerId <- payerId, self.payerName <- payerName, self.participantsId <- participantsId, self.participantsName <- participantsName, self.participantsExtraAmount <- participantsExtraAmount)
        do {
            try self.database.run(updateGroupExpense )
        } catch {
            print(error)
        }
    }
    
    func getParticipantsName(names: String, extraAmounts: String) -> String{
        let namesArr = names.components(separatedBy: ",")
        let extraAmountsArr = extraAmounts.components(separatedBy: ",")
        var participantsName = ""
        for i in 0..<namesArr.count{
            if extraAmountsArr[i] != "0"{
                participantsName += participantsName == "" ? "\(namesArr[i]) + \(extraAmountsArr[i])" : ", \(namesArr[i]) + \(extraAmountsArr[i])"
            }else{
                participantsName += participantsName == "" ? namesArr[i] : ", \(namesArr[i])"
            }
        }
        return participantsName
    }
    
    //MARK: - Validator
    func validator(groupExpenseModel: GroupExpensesModel) -> ValidatingExpenseResult {
        debugPrint(groupExpenseModel)
        if groupExpenseModel.dateCreate.isEmpty {
            return .failed(reason: "Please select date.")
        } else if groupExpenseModel.description.isEmpty {
            return .failed(reason: "Description is required.")
        } else if groupExpenseModel.amount == 0 {
            return .failed(reason: "Total amount is required and number.")
        } else if groupExpenseModel.participantsId.components(separatedBy: ",").count < 2 || groupExpenseModel.participantsId.isEmpty {
            return .failed(reason: "Please select participants is required and must more than 2 participants.")
        } else if groupExpenseModel.payerId == 0 {
            return .failed(reason: "Payer is required.")
        } else if groupExpenseModel.participantsId.components(separatedBy: ",").firstIndex(of: "\(groupExpenseModel.payerId)") == nil {
            return .failed(reason: "Payer must joint in date.")
        }
        return .passed(groupExpenseModel: groupExpenseModel)
    }
}
