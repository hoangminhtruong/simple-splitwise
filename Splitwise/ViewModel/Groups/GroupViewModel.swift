//
//  GroupViewModel.swift
//  Splitwise
//
//  Created by Hoang Minh Truong on 3/29/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation
import SQLite
import RxSwift
import RxCocoa

enum ValidatingGroupResult {
    case passed(groupModel: GroupModel)
    case failed(reason: String)
    
    static func ==(lhs: ValidatingGroupResult, rhs: ValidatingGroupResult) -> Bool {
        switch (lhs, rhs) {
        case (let .failed(reason1), let .failed(reason2)):
            return reason1 == reason2
        default:
            return false
        }
    }
}

class GroupViewModel {
    var groups: BehaviorRelay<[GroupModel]> = BehaviorRelay(value: [])
    var database: Connection!
    
    let groupsTable = Table("groups")
    let id = Expression<Int>("id")
    let description = Expression<String>("description")
    let dateCreated = Expression<String>("date_created")
    let participantsId = Expression<String>("participants_id")
    let participantsName = Expression<String>("participants_name")
    
    init() {
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent("groups").appendingPathExtension("sqlite3")
            let database = try Connection(fileUrl.path)
            self.database = database
        } catch {
            print(error)
        }
        create()
    }
    
    func create() {
        let createTable = self.groupsTable.create { (table) in
            table.column(self.id, primaryKey: true)
            table.column(self.description)
            table.column(self.dateCreated)
            table.column(self.participantsId)
            table.column(self.participantsName)
        }
        
        do {
            try self.database.run(createTable)
            print("Create successful")
        } catch {
            print(error)
        }
    }
    
    func fetch() {
        groups.accept([])
        do {
            let groups = try self.database.prepare(self.groupsTable)
            var groupsModel : [GroupModel] = []
            for group in groups {
                print("groupId: \(group[self.id]), description: \(group[self.description]), date_created: \(group[self.dateCreated])")
                groupsModel.append(GroupModel(id: group[self.id], description: group[self.description], dateCreate: group[self.dateCreated],  participantsId: group[self.participantsId], participantsName: group[self.participantsName]))
            }
            self.groups.accept(groupsModel)
        } catch {
            print(error)
        }
    }
    
    func insert(description: String,participantsId: String, participantsName: String) {
        let dateString = Date().convertToFormat(format: "MMM dd")
        let insertGroup = self.groupsTable.insert(self.description <- description,self.dateCreated <- dateString,self.participantsId <- participantsId, self.participantsName <- participantsName)
        do {
            try self.database.run(insertGroup)
            self.fetch()
            print("Insert successful")
        } catch {
            print(error)
        }
    }
    
    func delete(id: Int) {
        let group = self.groupsTable.filter(self.id == id)
        let deleteGroup = group.delete()
        do {
            try self.database.run(deleteGroup)
        } catch {
            print(error)
        }
    }
    
    func update(id: Int, name: String, description: String, participantsId: String, participantsName: String) {
        let group = self.groupsTable.filter(self.id == id)
        let dateString = Date().convertToFormat(format: "MMM dd")
        let updateGroup = group.update(self.description <- description,self.dateCreated <- dateString, self.participantsId <- participantsId, self.participantsName <- participantsName)
        do {
            try self.database.run(updateGroup)
        } catch {
            print(error)
        }
    }
    
    //MARK: - Validator
    func validator(groupModel: GroupModel) -> ValidatingGroupResult {
        if groupModel.participantsId.components(separatedBy: ",").count < 2 || groupModel.participantsId.isEmpty {
            return .failed(reason: "Please select participants is required and must more than 2 participants.")
        }
        return .passed(groupModel: groupModel)
    }
}
